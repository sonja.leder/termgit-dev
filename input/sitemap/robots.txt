User-agent: *
Disallow: /*.previous-versions.html
Disallow: /*.download.html
Disallow: /*.change.history.html
Allow: /CodeSystem-*
Allow: /ValueSet-*
Allow: /faq_*.html
Allow: /documentation_and_support_*.html
Allow: /index_en.html
Allow: /index.html
Disallow: /

Sitemap: https://termgit.elga.gv.at/sitemap.xml